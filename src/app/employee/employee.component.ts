import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  formEditEmployee = new FormGroup({});
  formEditInvalid = false;
  listJobTitle = [
    {id: 1, name: "net1"},
    {id: 2, name: "net2"},
    {id: 3, name: "net3"},
    {id: 4, name: "net4"},
    {id: 5, name: "net5"},
  ]

  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.createForm();
  }


  createForm() {
    this.formEditEmployee = this._formBuilder.group({
      fullName: ['gdsag', [Validators.required]],
      account: ['', Validators.required],
      doB: ['2019-07-20', Validators.required],
      phone: ['', Validators.required],
      address: ['', Validators.required],
      jobTitle: [4, [Validators.required]]
    });
  }

  onFormSubmit(form: FormGroup) {

    if (form.status == "INVALID") {
      this.formEditInvalid = true;
      return;
    }
    this.formEditInvalid = false;
    console.log(form);
  }
}
